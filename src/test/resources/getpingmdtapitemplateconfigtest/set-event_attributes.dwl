{
  "headers": {
    "authorization": "Bearer h0r8Kx343wa-GUPGvy-H2rGlaQNWsM8sGuOpsNAkXOId3nz9DQXVq_WaGLiS1epXZ2EVhHmN9jm4owJC-HfKZQ",
    "client-id": "7ca53be88fa44fb8b6f5080d77d64837",
    "x-correlation-id": "31c451d5-3ea9-46fc-9927-bc073fd97a9a",
    "user-agent": "PostmanRuntime/7.28.0",
    "accept": "*/*",
    "postman-token": "ca82a6f8-82f3-4877-8f60-8684fce71af5",
    "host": "localhost:8092",
    "accept-encoding": "gzip, deflate, br",
    "connection": "keep-alive"
  },
  "clientCertificate": null,
  "method": "GET",
  "scheme": "https",
  "queryParams": {},
  "requestUri": "/api/ping",
  "queryString": "",
  "version": "HTTP/1.1",
  "maskedRequestPath": "/ping",
  "listenerPath": "/api/*",
  "relativePath": "/api/ping",
  "localAddress": "/127.0.0.1:8092",
  "uriParams": {},
  "rawRequestUri": "/api/ping",
  "rawRequestPath": "/api/ping",
  "remoteAddress": "/127.0.0.1:60142",
  "requestPath": "/api/ping"
}